package br.com.mvsistema.ofertaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.Notificacao;
import br.com.mvsistema.ofertaapi.repository.helper.NotificacaoHelper;



@Repository
public interface NotificacaoRepository extends JpaRepository<Notificacao, Long>,NotificacaoHelper {

	

}

