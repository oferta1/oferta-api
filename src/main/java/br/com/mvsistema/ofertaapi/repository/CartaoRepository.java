package br.com.mvsistema.ofertaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.Cartao;
import br.com.mvsistema.ofertaapi.repository.helper.CartaoHelper;



@Repository
public interface CartaoRepository extends JpaRepository<Cartao, Long>,CartaoHelper {

}

