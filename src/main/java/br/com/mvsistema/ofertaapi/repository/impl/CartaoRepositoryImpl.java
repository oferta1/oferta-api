package br.com.mvsistema.ofertaapi.repository.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.ofertaapi.repository.helper.CartaoHelper;
import br.com.mvsistema.ofertaapi.filter.CartaoFilter;
import br.com.mvsistema.ofertaapi.model.Cartao;

public class CartaoRepositoryImpl implements CartaoHelper {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Cartao> filtrar(CartaoFilter cartaoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Cartao> criteria = builder.createQuery(Cartao.class);
		Root<Cartao> root = criteria.from(Cartao.class);

		Predicate[] predicates = criarRestrincoes(cartaoFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Cartao> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(cartaoFilter));
	}

	private Predicate[] criarRestrincoes(CartaoFilter cartaoFilter, CriteriaBuilder builder, Root<Cartao> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (cartaoFilter != null) {

			if (cartaoFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), cartaoFilter.getId()));
			}

			if (!StringUtils.isEmpty(cartaoFilter.getNumeroCartao())) {
				predicates.add(builder.like(builder.lower(root.get("numeroCartao")),
						"%" + cartaoFilter.getNumeroCartao() + "%"));
			}

			if (!StringUtils.isEmpty(cartaoFilter.getSenhaCatao())) {
				predicates.add(builder.like(builder.lower(root.get("senhaCatao")),
						"%" + cartaoFilter.getSenhaCatao().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(cartaoFilter.getNomeCliente())) {
				predicates.add(builder.like(builder.lower(root.get("nomeCliente")),
						"%" + cartaoFilter.getNomeCliente().toLowerCase() + "%"));
			}

			if (cartaoFilter.getValorFatura() != null) {
				predicates.add(builder.equal(root.get("valorFatura"), cartaoFilter.getValorFatura()));
			}

			if (cartaoFilter.getLimite() != null) {
				predicates.add(builder.equal(root.get("limite"), cartaoFilter.getLimite()));
			}

			if (cartaoFilter.getLimiteDisponivel() != null) {
				predicates.add(builder.equal(root.get("limiteDisponivel"), cartaoFilter.getLimiteDisponivel()));
			}

			if (cartaoFilter.getUsuarioCartao() != null) {
				predicates.add(builder.equal(root.get("usuarioCartao"), cartaoFilter.getUsuarioCartao()));
			}

			// predicates.add(builder.equal(root.get("status"), cartaoFilter.isStatus()));

		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}

	private Long total(CartaoFilter cartaoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Cartao> root = criteria.from(Cartao.class);

		Predicate[] predicates = criarRestrincoes(cartaoFilter, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public Cartao findByCartaoInUsuarioCpf(String cpf) {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT c.id_cartao as id, ");
		sb.append("       c.banco_cartao as bancoCartao, ");
		sb.append("       c.expira_em as expiraEm, ");
		sb.append("       c.limite as limite, ");
		sb.append("       c.limite_disponivel as limiteDisponivel, ");
		sb.append("       c.status as status, ");
		sb.append("       c.logo_cartao as logoCartao, ");
		sb.append("       c.nome_cliente as nomeCliente, ");
		sb.append("       c.numero_cartao as numeroCartao, ");
		sb.append("       c.senha_catao as senhaCartao, ");
		sb.append("       c.valor_fatura as valorFatura ");
		sb.append("FROM cartao c ");
		sb.append("INNER JOIN usuario u ON c.id_usuario = u.id_usuario ");
		sb.append("WHERE 1=1 ");
		sb.append("  AND u.cpf = :cpf");
		
		Cartao cartao = null;

		try {
			Query query = (Query) manager.createNativeQuery(sb.toString());
			query.setParameter("cpf", cpf);
			
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			for (Object obj : query.getResultList()) {
				Map o = (Map) obj;
				cartao = new Cartao();
				cartao.setId(((BigInteger)o.get("id")).longValue());
				cartao.setBancoCartao((String) o.get("bancoCartao"));
				cartao.setExpiraEm((String) o.get("expiraEm"));
				cartao.setLimite( (BigDecimal) o.get("limite") );
				cartao.setLimiteDisponivel( (BigDecimal) o.get("limiteDisponivel") );
				cartao.setStatus((boolean) o.get("status"));
				cartao.setLogoCartao((String) o.get("logoCartao"));
				cartao.setNomeCliente((String) o.get("nomeCliente"));
				cartao.setNumeroCartao((String) o.get("numeroCartao"));
				cartao.setSenhaCatao((String) o.get("senhaCartao"));
				cartao.setValorFatura( (BigDecimal) o.get("valorFatura") );
			}

			return cartao;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return cartao;
		}
	}
}
