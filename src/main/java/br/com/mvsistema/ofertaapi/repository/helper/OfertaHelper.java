package br.com.mvsistema.ofertaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.ofertaapi.filter.OfertaFilter;
import br.com.mvsistema.ofertaapi.model.Oferta;



public interface OfertaHelper {

	public Page<Oferta> filtrar(OfertaFilter ofertaFilter, Pageable pageable);

}
