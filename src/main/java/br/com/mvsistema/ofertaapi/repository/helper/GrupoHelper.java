package br.com.mvsistema.ofertaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.ofertaapi.filter.GrupoFilter;
import br.com.mvsistema.ofertaapi.model.Grupo;



public interface GrupoHelper {

	public Page<Grupo> filtrar(GrupoFilter grupoFilter, Pageable pageable);

}
