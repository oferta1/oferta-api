package br.com.mvsistema.ofertaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.ofertaapi.repository.helper.GrupoHelper;
import br.com.mvsistema.ofertaapi.filter.GrupoFilter;
import br.com.mvsistema.ofertaapi.model.Grupo;


public class GrupoRepositoryImpl implements GrupoHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Grupo> filtrar(GrupoFilter  grupoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Grupo> criteria = builder.createQuery(Grupo.class);
		Root<Grupo> root = criteria.from(Grupo.class);
		
		Predicate[] predicates = criarRestrincoes(grupoFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Grupo> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(grupoFilter));
	}
	
	
	private Predicate[] criarRestrincoes(GrupoFilter  grupoFilter, CriteriaBuilder builder, Root<Grupo> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (grupoFilter != null) {
			
                      if (grupoFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), grupoFilter.getId()));
			}

           if (!StringUtils.isEmpty(grupoFilter.getNome())) {				
				predicates.add(builder.like(builder.lower(root.get("nome")),"%" + grupoFilter.getNome().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(grupoFilter.getDescricao())) {				
				predicates.add(builder.like(builder.lower(root.get("descricao")),"%" + grupoFilter.getDescricao().toLowerCase() + "%"));
			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(GrupoFilter  grupoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Grupo> root = criteria.from(Grupo.class);
		
		Predicate[] predicates = criarRestrincoes(grupoFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
