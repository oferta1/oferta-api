package br.com.mvsistema.ofertaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.ofertaapi.repository.helper.OfertaHelper;
import br.com.mvsistema.ofertaapi.filter.OfertaFilter;
import br.com.mvsistema.ofertaapi.model.Oferta;


public class OfertaRepositoryImpl implements OfertaHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Oferta> filtrar(OfertaFilter  ofertaFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Oferta> criteria = builder.createQuery(Oferta.class);
		Root<Oferta> root = criteria.from(Oferta.class);
		
		Predicate[] predicates = criarRestrincoes(ofertaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Oferta> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(ofertaFilter));
	}
	
	
	private Predicate[] criarRestrincoes(OfertaFilter  ofertaFilter, CriteriaBuilder builder, Root<Oferta> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (ofertaFilter != null) {
			
                      if (ofertaFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), ofertaFilter.getId()));
			}

           if (!StringUtils.isEmpty(ofertaFilter.getNomeProduto())) {				
				predicates.add(builder.like(root.get("nomeProduto"),"%" + ofertaFilter.getNomeProduto() + "%"));
			}
			

           if (!StringUtils.isEmpty(ofertaFilter.getDescricao())) {				
				predicates.add(builder.like(root.get("descricao"),"%" + ofertaFilter.getDescricao() + "%"));
			}
			

            if (ofertaFilter.getPrecoOferta() != null) {
				predicates.add(builder.equal(root.get("precoOferta"), ofertaFilter.getPrecoOferta()));
			}

            if (ofertaFilter.getPrecoAnterior() != null) {
				predicates.add(builder.equal(root.get("precoAnterior"), ofertaFilter.getPrecoAnterior()));
			}

           if (!StringUtils.isEmpty(ofertaFilter.getImagem())) {				
				predicates.add(builder.like(builder.lower(root.get("imagem")),"%" + ofertaFilter.getImagem().toLowerCase() + "%"));
			}
			

            if (ofertaFilter.getPeriodoOferta() != null) {
				predicates.add(builder.equal(root.get("periodoOferta"), ofertaFilter.getPeriodoOferta()));
			}



		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(OfertaFilter  ofertaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Oferta> root = criteria.from(Oferta.class);
		
		Predicate[] predicates = criarRestrincoes(ofertaFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
