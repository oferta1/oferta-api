package br.com.mvsistema.ofertaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.Grupo;
import br.com.mvsistema.ofertaapi.repository.helper.GrupoHelper;



@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Long>,GrupoHelper {

}

