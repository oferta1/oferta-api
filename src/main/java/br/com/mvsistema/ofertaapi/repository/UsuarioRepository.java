package br.com.mvsistema.ofertaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.Usuario;
import br.com.mvsistema.ofertaapi.repository.helper.UsuarioHelper;



@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>,UsuarioHelper {

	@Query(value = "select u from usuario u where u.cpf = :cpf")
	public Usuario findByCpf(@Param("cpf") String cpf);
}

