package br.com.mvsistema.ofertaapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.Permissao;
import br.com.mvsistema.ofertaapi.repository.helper.PermissaoHelper;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long>,PermissaoHelper {

	@Query(nativeQuery = true, 
			value = "SELECT p.id_permissao, p.nome FROM permisssao p \n" + 
					"	INNER JOIN usuario_permissoes up \n" + 
					"		ON p.id_permissao = up.id_permissao \n" + 
					"	INNER JOIN usuario u \n" + 
					"		ON u.id_usuario = up.id_usuario \n" + 
					"WHERE u.cpf = ?")
	public List<Permissao> listaPermissoes(String cpf);
}

