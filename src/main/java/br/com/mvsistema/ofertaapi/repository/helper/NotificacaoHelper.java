package br.com.mvsistema.ofertaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.ofertaapi.filter.NotificacaoFilter;
import br.com.mvsistema.ofertaapi.model.Notificacao;



public interface NotificacaoHelper {

	public Page<Notificacao> filtrar(NotificacaoFilter notificacaoFilter, Pageable pageable);

}
