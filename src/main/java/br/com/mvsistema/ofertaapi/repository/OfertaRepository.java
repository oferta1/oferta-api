package br.com.mvsistema.ofertaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.Oferta;
import br.com.mvsistema.ofertaapi.repository.helper.OfertaHelper;



@Repository
public interface OfertaRepository extends JpaRepository<Oferta, Long>,OfertaHelper {

	

}

