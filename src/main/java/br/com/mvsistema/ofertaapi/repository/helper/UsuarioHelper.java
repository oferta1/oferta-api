package br.com.mvsistema.ofertaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.ofertaapi.filter.UsuarioFilter;
import br.com.mvsistema.ofertaapi.model.Usuario;



public interface UsuarioHelper {

	public Page<Usuario> filtrar(UsuarioFilter usuarioFilter, Pageable pageable);

}
