package br.com.mvsistema.ofertaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.ofertaapi.filter.CartaoFilter;
import br.com.mvsistema.ofertaapi.model.Cartao;



public interface CartaoHelper {

	public Page<Cartao> filtrar(CartaoFilter cartaoFilter, Pageable pageable);
	public Cartao findByCartaoInUsuarioCpf(String cpf);

}
