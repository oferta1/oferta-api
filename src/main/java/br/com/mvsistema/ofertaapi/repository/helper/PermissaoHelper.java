package br.com.mvsistema.ofertaapi.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import br.com.mvsistema.ofertaapi.filter.PermissaoFilter;
import br.com.mvsistema.ofertaapi.model.Permissao;



public interface PermissaoHelper {

	public Page<Permissao> filtrar(PermissaoFilter permissaoFilter, Pageable pageable);

}
