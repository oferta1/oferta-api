package br.com.mvsistema.ofertaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.ofertaapi.repository.helper.SolicitacaoCartaoHelper;
import br.com.mvsistema.ofertaapi.filter.SolicitacaoCartaoFilter;
import br.com.mvsistema.ofertaapi.model.SolicitacaoCartao;


public class SolicitacaoCartaoRepositoryImpl implements SolicitacaoCartaoHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<SolicitacaoCartao> filtrar(SolicitacaoCartaoFilter  solicitacaoCartaoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<SolicitacaoCartao> criteria = builder.createQuery(SolicitacaoCartao.class);
		Root<SolicitacaoCartao> root = criteria.from(SolicitacaoCartao.class);
		
		Predicate[] predicates = criarRestrincoes(solicitacaoCartaoFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<SolicitacaoCartao> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(solicitacaoCartaoFilter));
	}
	
	
	private Predicate[] criarRestrincoes(SolicitacaoCartaoFilter  solicitacaoCartaoFilter, CriteriaBuilder builder, Root<SolicitacaoCartao> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (solicitacaoCartaoFilter != null) {
			
                      if (solicitacaoCartaoFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), solicitacaoCartaoFilter.getId()));
			}

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getNome())) {				
				predicates.add(builder.like(builder.lower(root.get("nome")),"%" + solicitacaoCartaoFilter.getNome().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getEmail())) {				
				predicates.add(builder.like(builder.lower(root.get("email")),"%" + solicitacaoCartaoFilter.getEmail().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getCpf())) {				
				predicates.add(builder.like(builder.lower(root.get("cpf")),"%" + solicitacaoCartaoFilter.getCpf().toLowerCase() + "%"));
			}
			

            if (solicitacaoCartaoFilter.getDataNascimento() != null) {
				predicates.add(builder.equal(root.get("dataNascimento"), solicitacaoCartaoFilter.getDataNascimento()));
			}

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getTelefone())) {				
				predicates.add(builder.like(builder.lower(root.get("telefone")),"%" + solicitacaoCartaoFilter.getTelefone().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getProfissao())) {				
				predicates.add(builder.like(builder.lower(root.get("profissao")),"%" + solicitacaoCartaoFilter.getProfissao().toLowerCase() + "%"));
			}
			

            if (solicitacaoCartaoFilter.getRendaMensal() != null) {
				predicates.add(builder.equal(root.get("rendaMensal"), solicitacaoCartaoFilter.getRendaMensal()));
			}

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getDocumento())) {				
				predicates.add(builder.like(builder.lower(root.get("documento")),"%" + solicitacaoCartaoFilter.getDocumento().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getCep())) {				
				predicates.add(builder.like(builder.lower(root.get("cep")),"%" + solicitacaoCartaoFilter.getCep().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getLogradouro())) {				
				predicates.add(builder.like(builder.lower(root.get("logradouro")),"%" + solicitacaoCartaoFilter.getLogradouro().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getComplemento())) {				
				predicates.add(builder.like(builder.lower(root.get("complemento")),"%" + solicitacaoCartaoFilter.getComplemento().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getBairro())) {				
				predicates.add(builder.like(builder.lower(root.get("bairro")),"%" + solicitacaoCartaoFilter.getBairro().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getLocalidade())) {				
				predicates.add(builder.like(builder.lower(root.get("localidade")),"%" + solicitacaoCartaoFilter.getLocalidade().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getNumero())) {				
				predicates.add(builder.like(builder.lower(root.get("numero")),"%" + solicitacaoCartaoFilter.getNumero().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(solicitacaoCartaoFilter.getUf())) {				
				predicates.add(builder.like(builder.lower(root.get("uf")),"%" + solicitacaoCartaoFilter.getUf().toLowerCase() + "%"));
			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(SolicitacaoCartaoFilter  solicitacaoCartaoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<SolicitacaoCartao> root = criteria.from(SolicitacaoCartao.class);
		
		Predicate[] predicates = criarRestrincoes(solicitacaoCartaoFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
