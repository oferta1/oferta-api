package br.com.mvsistema.ofertaapi.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.mvsistema.ofertaapi.repository.helper.NotificacaoHelper;
import br.com.mvsistema.ofertaapi.filter.NotificacaoFilter;
import br.com.mvsistema.ofertaapi.model.Notificacao;


public class NotificacaoRepositoryImpl implements NotificacaoHelper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Notificacao> filtrar(NotificacaoFilter  notificacaoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Notificacao> criteria = builder.createQuery(Notificacao.class);
		Root<Notificacao> root = criteria.from(Notificacao.class);
		
		Predicate[] predicates = criarRestrincoes(notificacaoFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Notificacao> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(notificacaoFilter));
	}
	
	
	private Predicate[] criarRestrincoes(NotificacaoFilter  notificacaoFilter, CriteriaBuilder builder, Root<Notificacao> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (notificacaoFilter != null) {
			
                      if (notificacaoFilter.getId() != null) {
				predicates.add(builder.equal(root.get("id"), notificacaoFilter.getId()));
			}

           if (!StringUtils.isEmpty(notificacaoFilter.getNome())) {				
				predicates.add(builder.like(builder.lower(root.get("nome")),"%" + notificacaoFilter.getNome().toLowerCase() + "%"));
			}
			

           if (!StringUtils.isEmpty(notificacaoFilter.getDescricao())) {				
				predicates.add(builder.like(builder.lower(root.get("descricao")),"%" + notificacaoFilter.getDescricao().toLowerCase() + "%"));
			}
			

            if (notificacaoFilter.getData() != null) {
				predicates.add(builder.equal(root.get("data"), notificacaoFilter.getData()));
			}



		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(NotificacaoFilter  notificacaoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Notificacao> root = criteria.from(Notificacao.class);
		
		Predicate[] predicates = criarRestrincoes(notificacaoFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
