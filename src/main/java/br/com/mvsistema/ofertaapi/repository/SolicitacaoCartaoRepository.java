package br.com.mvsistema.ofertaapi.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvsistema.ofertaapi.model.SolicitacaoCartao;
import br.com.mvsistema.ofertaapi.repository.helper.SolicitacaoCartaoHelper;



@Repository
public interface SolicitacaoCartaoRepository extends JpaRepository<SolicitacaoCartao, Long>,SolicitacaoCartaoHelper {

	

}

