package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "grupo")
public class Grupo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6943009508134012882L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_grupo", insertable=false, updatable = false)
	private Long id;
	
	private String nome;
	
	private String descricao;
	
	@ManyToMany
	@JoinTable(name = "grupo_permissoes", joinColumns = @JoinColumn(name = "grupos_id_grupo"), inverseJoinColumns = @JoinColumn(name = "permissoes_id_permissao"))
	private List<Permissao> permissoes;
	
	public Grupo() {
	}

	public Grupo(Long id, String nome, String descricao, List<Permissao> permissoes) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.permissoes = permissoes;
	}
}
