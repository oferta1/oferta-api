package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Classe responsavel pela Oferta
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 13 de nov de 2019
 */
@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "oferta")
public class Oferta implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8883967675176312709L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_oferta", insertable=false, updatable = false)
	private Long id;
	private String nomeProduto;
	private String descricao;
	@Column(precision = 19, scale = 6)
	private BigDecimal precoOferta;
	@Column(precision = 19, scale = 6)
	private BigDecimal precoAnterior;
	@Lob
	private String imagem;
	@Enumerated(EnumType.STRING)
	private PeriodoOferta periodoOferta;
	
	public Oferta() {
	}

	public Oferta(Long id, String nomeProduto, String descricao, BigDecimal precoOferta, BigDecimal precoAnterior,
			String imagem, PeriodoOferta periodoOferta) {
		super();
		this.id = id;
		this.nomeProduto = nomeProduto;
		this.descricao = descricao;
		this.precoOferta = precoOferta;
		this.precoAnterior = precoAnterior;
		this.imagem = imagem;
		this.periodoOferta = periodoOferta;
	}
}
