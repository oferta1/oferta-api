package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "usuario")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6053688454250377425L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario", insertable=false, updatable = false)
	private Long id;
	private String nome;
	@CPF
	@Column(unique = true)
	private String cpf;
	@Email
	private String email;
	private String senha;
	private boolean ativo;
	@Transient
	private String token;
	@Transient
	private Long idPermissao;
	@Transient
	private List<String> auths = new ArrayList<String>();
	@Transient
	private Collection<? extends GrantedAuthority> authorities;
	
	@JsonProperty("permissoes")
	@ManyToMany()
	@JoinTable(name = "usuario_permissoes", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_permissao"))
	private List<Permissao> permissoes;
	
	public Usuario() {
	}

	public Usuario(Long id, String nome, String cpf, String email, String senha, boolean ativo, List<Permissao> permissoes) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.senha = senha;
		this.ativo = ativo;
		this.permissoes = permissoes;
	}
	
}
