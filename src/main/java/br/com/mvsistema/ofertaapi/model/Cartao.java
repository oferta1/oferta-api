package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Classe que Gerencia o Cartão
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 13 de nov de 2019
 */
@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "cartao")
public class Cartao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2459747789030945236L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cartao", insertable=false, updatable = false)
	private Long id;
	@Column(unique = true)
	private String numeroCartao;
	private String senhaCatao;
	private String nomeCliente;
	@Column(precision = 19, scale = 6)
	private BigDecimal valorFatura;
	@Column(precision = 19, scale = 6)
	private BigDecimal limite;
	@Column(precision = 19, scale = 6)
	private BigDecimal limiteDisponivel;
	private boolean status;
	@OneToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuarioCartao;
	@Lob
	private String logoCartao;
	private String expiraEm;
	private String bancoCartao;
	
	public Cartao() {
	}

	public Cartao(Long id, String numeroCartao, String senhaCatao, String nomeCliente, BigDecimal valorFatura,
			BigDecimal limite, BigDecimal limiteDisponivel, boolean status, Usuario usuarioCartao, String logoCartao, String expiraEm, String bancoCartao) {
		super();
		this.id = id;
		this.numeroCartao = numeroCartao;
		this.senhaCatao = senhaCatao;
		this.nomeCliente = nomeCliente;
		this.valorFatura = valorFatura;
		this.limite = limite;
		this.limiteDisponivel = limiteDisponivel;
		this.status = status;
		this.usuarioCartao = usuarioCartao;
		this.logoCartao = logoCartao;
		this.bancoCartao = bancoCartao;
	}
}
