package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Classe que gerencia a Notificacao
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 13 de nov de 2019
 */
@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "notificacao")
public class Notificacao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 419786821209411056L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_notificacao", insertable=false, updatable = false)
	private Long id;
	private String nome;
	private String descricao;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	private LocalDateTime dataNotificacao;
	
	public Notificacao() {
	}

	public Notificacao(Long id, String nome, String descricao, LocalDateTime dataNotificacao) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.dataNotificacao = dataNotificacao;
	}
}
