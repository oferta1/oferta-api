package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Classe para gerenciar a solicitacao do cartão
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 13 de nov de 2019
 */
@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "solicitacao_cartao")
public class SolicitacaoCartao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3990672746059598549L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_solicitacao_cartao", insertable=false, updatable = false)
	private Long id;
	private String nome;
	@Email(message="Por favor, forneça um endereço de e-mail válido")
	private String email;
	@CPF
	@Column(unique = true)
	private String cpf;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
	private LocalDateTime dataNascimento;
	private String telefone;
	private String profissao;
	@Column(precision = 19, scale = 6)
	private BigDecimal rendaMensal;
	@Lob
	private String documento;
	private String cep;
	private String logradouro;
	private String complemento;
	private String bairro;
	private String localidade;
	private String numero;
	private String uf;
	private boolean status;
//	@ManyToOne
//	@JoinColumn(name="id_usuario_solicitante")
//	private Usuario usuarioSolicitante;
	
	public SolicitacaoCartao() {
	}

	public SolicitacaoCartao(Long id, String nome, String email, String cpf, LocalDateTime dataNascimento, String telefone,
			String profissao, BigDecimal rendaMensal, String documento, String cep, String logradouro,
			String complemento, String bairro, String localidade, String numero, String uf, boolean status) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
		this.telefone = telefone;
		this.profissao = profissao;
		this.rendaMensal = rendaMensal;
		this.documento = documento;
		this.cep = cep;
		this.logradouro = logradouro;
		this.complemento = complemento;
		this.bairro = bairro;
		this.localidade = localidade;
		this.numero = 
		this.uf = uf;
		this.status = status;
	}

	
}
