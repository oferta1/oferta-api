package br.com.mvsistema.ofertaapi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "permisssao")
public class Permissao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1856975309079281259L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_permissao", insertable=false, updatable = false)
	private Long id;

	private String nome;
	
	public Permissao() {
	}

	public Permissao(Long id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}
}
