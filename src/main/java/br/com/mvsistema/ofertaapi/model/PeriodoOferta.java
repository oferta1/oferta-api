package br.com.mvsistema.ofertaapi.model;

public enum PeriodoOferta {
	
	PERIODO_SEMANA("Oferta da Semana"),
	PERIODO_DIA("oferta do Dia"),
	PERIODO_MES("Oferta do Mês");
	
	private String descricao;
	
	PeriodoOferta(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
