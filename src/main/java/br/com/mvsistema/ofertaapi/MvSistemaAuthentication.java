package br.com.mvsistema.ofertaapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mvsistema.ofertaapi.model.Permissao;
import br.com.mvsistema.ofertaapi.model.Usuario;
import br.com.mvsistema.ofertaapi.repository.PermissaoRepository;
import br.com.mvsistema.ofertaapi.repository.UsuarioRepository;
import lombok.var;

public class MvSistemaAuthentication extends AbstractAuthenticationProcessingFilter {
	
	protected MvSistemaAuthentication(String url, AuthenticationManager manager) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(manager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		
		Usuario usuario = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);

		return getAuthenticationManager().authenticate(
				new UsernamePasswordAuthenticationToken(usuario.getCpf(), usuario.getSenha(), new ArrayList<>()));
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {

		Usuario usuario = new Usuario();
		usuario.setCpf(authResult.getName());
		
	    for (GrantedAuthority a : authResult.getAuthorities()) {
	        usuario.getAuths().add(a.getAuthority());
	    }
		
		usuario.setAuthorities(authResult.getAuthorities());
		
		usuario.setToken(MvSistemaToken.geraToken(usuario.getCpf()));
		
		String usuarioJson = new ObjectMapper().writeValueAsString(usuario);
		
		response.getWriter().write(usuarioJson);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		
		
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Cpf e/ou Senha e/ou permissão inválidos");
	}
	
	
}
