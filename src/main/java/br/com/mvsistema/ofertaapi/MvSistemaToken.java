package br.com.mvsistema.ofertaapi;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.mvsistema.ofertaapi.model.Usuario;
import br.com.mvsistema.ofertaapi.repository.UsuarioRepository;
import br.com.mvsistema.ofertaapi.service.UsuarioService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Classe responsavel por gerar e verificar o token
 * 
 * @author viniciusluciene MVSistema - Todos os Direitos reservados. 21 de nov
 *         de 2019
 */
public class MvSistemaToken {

	private static final String CHAVE = "MvSistemas - Chave Token";
	private static Calendar calendario1 = Calendar.getInstance();

	public static String geraToken(String subject) {
		calendario1.set(2050, 04, 01, 00, 00, 00);
		String token = Jwts.builder().setSubject(subject)
				.setExpiration(new Date(calendario1.getTimeInMillis() + (60 * 60000)))
				.signWith(SignatureAlgorithm.HS512, CHAVE).compact();

		return token;
	}

	public static String verificaToken(String token) throws Exception {
		if (token != null) {
			try {
				return Jwts.parser().setSigningKey(CHAVE).parseClaimsJws(token).getBody().getSubject();

			} catch (Exception e) {
				throw new Exception("Token inválido ou expirado");
			}
		}
		throw new Exception("Necessário informar o token");
	}
}
