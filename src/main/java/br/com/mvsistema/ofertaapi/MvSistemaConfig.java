package br.com.mvsistema.ofertaapi;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class MvSistemaConfig extends WebSecurityConfigurerAdapter {
	
	private static final String USUARIO_POR_LOGIN_COM_PERMISSAO = "SELECT cpf, senha, ativo as enabled \n" + 
			"	FROM usuario u \n" + 
			"		INNER JOIN usuario_permissoes up \n" + 
			"			ON u.id_usuario = up.id_usuario \n" + 
			"	WHERE cpf = ? \n" + 
			"	AND up.id_permissao <> 4";
	
	private static final String USUARIO_POR_CARTAO = "SELECT u.cpf, \n" + 
			"	   u.senha, \n" + 
			"	   u.id_usuario, \n" + 
			"	   c.id_cartao  \n" + 
			"	FROM usuario u \n" + 
			"		LEFT JOIN cartao c \n" + 
			"			ON u.id_usuario = c.id_usuario"+
			"	WHERE u.cpf = ?";
	
	private static final String USUARIO_POR_LOGIN = "SELECT cpf, senha, id_usuario FROM usuario WHERE cpf = ?";

	private static final String PERMISSOES_POR_USUARIO = "SELECT u.cpf, p.nome FROM usuario_permissoes up"
			+ " JOIN usuario u ON u.id_usuario = up.id_usuario JOIN permisssao p ON p.id_permissao = up.id_permissao"
			+ " WHERE u.cpf = ?";

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests().antMatchers("/login").permitAll().antMatchers("/error")
				.permitAll().anyRequest().authenticated().and()

				.addFilterBefore(new MvSistemaAuthentication("/login", authenticationManager()),
						UsernamePasswordAuthenticationFilter.class)

				.addFilterBefore(new JWTFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
				.dataSource(dataSource)
				.usersByUsernameQuery(USUARIO_POR_LOGIN)
				.authoritiesByUsernameQuery(PERMISSOES_POR_USUARIO)
				.rolePrefix("ROLE_")
				.passwordEncoder(new BCryptPasswordEncoder());
	}
}
