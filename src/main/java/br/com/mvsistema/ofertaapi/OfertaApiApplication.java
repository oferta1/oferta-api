package br.com.mvsistema.ofertaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class OfertaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfertaApiApplication.class, args);
		geraSenha();
	}

	private static void geraSenha() {
		System.out.println(new BCryptPasswordEncoder().encode("123"));
	}

}
