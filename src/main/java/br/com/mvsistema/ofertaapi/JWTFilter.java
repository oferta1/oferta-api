package br.com.mvsistema.ofertaapi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import br.com.mvsistema.ofertaapi.model.Permissao;
import br.com.mvsistema.ofertaapi.model.Usuario;
import br.com.mvsistema.ofertaapi.repository.PermissaoRepository;
import br.com.mvsistema.ofertaapi.repository.UsuarioRepository;

/**
 * Classe responsavel por pegar o token na requisicao
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 21 de nov de 2019
 */
public class JWTFilter extends GenericFilterBean {
	
	@Autowired private UsuarioRepository userRepository;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			String token = null;
			Authentication authentication = null;
			if (((HttpServletRequest) request).getHeader("Authorization") != null) {
				token = ((HttpServletRequest) request).getHeader("Authorization").replace("Bearer ", "");
			}

			if (token != null) {
				String login = MvSistemaToken.verificaToken(token);
				
				// Usuario userLogado = this.userRepository.findByCpf(login);

				authentication = new UsernamePasswordAuthenticationToken(login, null, new ArrayList<>());
			}

			SecurityContextHolder.getContext().setAuthentication(authentication);

			chain.doFilter(request, response);
		} catch (Exception e) {
			((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
		}

	}
}
