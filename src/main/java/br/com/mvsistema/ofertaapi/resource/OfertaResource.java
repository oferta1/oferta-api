
package br.com.mvsistema.ofertaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.OfertaFilter;
import br.com.mvsistema.ofertaapi.model.Oferta;
import br.com.mvsistema.ofertaapi.service.OfertaService;

@RestController
@RequestMapping("/oferta")
public class OfertaResource {
	
	@Autowired
	private OfertaService ofertaService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Oferta
	 */
	@GetMapping
	public Page<Oferta> search(OfertaFilter ofertaFilter, Pageable pageable) {
		return ofertaService.filter(ofertaFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Oferta
	 */
	@GetMapping("/{code}")
	public ResponseEntity<Oferta> buscarPeloCodigo(@PathVariable Long code) {
		Oferta oferta = ofertaService.buscarOfertaPeloCodigo(code);
		return oferta != null ? ResponseEntity.ok(oferta) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Oferta
	 */
	@PostMapping
	public ResponseEntity<Oferta> criar(@Valid @RequestBody Oferta oferta, HttpServletResponse response) {
		Oferta ofertaSalvo = ofertaService.save(oferta);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, ofertaSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(ofertaSalvo);
	}
	
	/**
	 * Atualizar Oferta
	 */
	@PutMapping("/{code}")
	public ResponseEntity<Oferta> atualizar(@PathVariable Long code, @Valid @RequestBody Oferta oferta) {
		Oferta ofertaSalva = ofertaService.atualizar(code, oferta);
		return ResponseEntity.ok(ofertaSalva);
	}
	

	/**
	 * Deletar Oferta
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		ofertaService.delete(code);
	}

}
