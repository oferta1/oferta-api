package br.com.mvsistema.ofertaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.UsuarioFilter;
import br.com.mvsistema.ofertaapi.model.Usuario;
import br.com.mvsistema.ofertaapi.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	/**
	 * Pesquisar Usuario
	 */
	@GetMapping
	public Page<Usuario> search(UsuarioFilter usuarioFilter, Pageable pageable) {
		return usuarioService.filter(usuarioFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Usuario
	 */
	@GetMapping("/{code}")
	public ResponseEntity<Usuario> buscarPeloCodigo(@PathVariable Long code) {
		Usuario usuario = usuarioService.buscarUsuarioPeloCodigo(code);
		return usuario != null ? ResponseEntity.ok(usuario) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Usuario
	 */
	@PostMapping
	public ResponseEntity<Usuario> criar(@Valid @RequestBody Usuario usuario, HttpServletResponse response) {
		Usuario usuarioSalvo = usuarioService.save(usuario);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, usuarioSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
	}
	
	/**
	 * Atualizar Usuario
	 */
	@PutMapping("/{code}")
	public ResponseEntity<Usuario> atualizar(@PathVariable Long code, @Valid @RequestBody Usuario usuario) {
		Usuario usuarioSalva = usuarioService.atualizar(code, usuario);
		return ResponseEntity.ok(usuarioSalva);
	}
	

	/**
	 * Deletar Usuario
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		usuarioService.delete(code);
	}

}
