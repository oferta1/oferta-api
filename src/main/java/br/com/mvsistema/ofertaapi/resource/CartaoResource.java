package br.com.mvsistema.ofertaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.CartaoFilter;
import br.com.mvsistema.ofertaapi.model.Cartao;
import br.com.mvsistema.ofertaapi.service.CartaoService;

@RestController
@RequestMapping("/cartao")
public class CartaoResource {
	
	@Autowired
	private CartaoService cartaoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Cartao
	 */
	@GetMapping
	public Page<Cartao> search(CartaoFilter cartaoFilter, Pageable pageable) {
		return cartaoService.filter(cartaoFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Cartao
	 */
	@GetMapping("/{code}")
	public ResponseEntity<Cartao> buscarPeloCodigo(@PathVariable Long code) {
		Cartao cartao = cartaoService.buscarCartaoPeloCodigo(code);
		return cartao != null ? ResponseEntity.ok(cartao) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Buscar pelo usario o Cartao
	 */
	@GetMapping("/usuario/{cpf}")
	public ResponseEntity<Cartao> buscarCartaoPeloCpfDoUsuario(@PathVariable String cpf) {
		Cartao cartao = cartaoService.buscarCartaoPeloCpfDoUsuario(cpf);
		return cartao != null ? ResponseEntity.ok(cartao) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Cartao
	 */
	@PostMapping
	public ResponseEntity<Cartao> criar(@Valid @RequestBody Cartao cartao, HttpServletResponse response) {
		Cartao cartaoSalvo = cartaoService.save(cartao);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, cartaoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(cartaoSalvo);
	}
	
	/**
	 * Atualizar Cartao
	 */
	@PutMapping("/{code}")
	public ResponseEntity<Cartao> atualizar(@PathVariable Long code, @Valid @RequestBody Cartao cartao) {
		Cartao cartaoSalva = cartaoService.atualizar(code, cartao);
		return ResponseEntity.ok(cartaoSalva);
	}
	

	/**
	 * Deletar Cartao
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		cartaoService.delete(code);
	}

}
