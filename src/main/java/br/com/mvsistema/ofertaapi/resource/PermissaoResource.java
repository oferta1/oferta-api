package br.com.mvsistema.ofertaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.PermissaoFilter;
import br.com.mvsistema.ofertaapi.model.Permissao;
import br.com.mvsistema.ofertaapi.service.PermissaoService;

@RestController
@RequestMapping("/permissao")
public class PermissaoResource {
	
	@Autowired
	private PermissaoService permissaoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Permissao
	 */
	@GetMapping
	public Page<Permissao> search(PermissaoFilter permissaoFilter, Pageable pageable) {
		return permissaoService.filter(permissaoFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Permissao
	 */
	@GetMapping("/{code}")
	public ResponseEntity<Permissao> buscarPeloCodigo(@PathVariable Long code) {
		Permissao permissao = permissaoService.buscarPermissaoPeloCodigo(code);
		return permissao != null ? ResponseEntity.ok(permissao) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Permissao
	 */
	@PostMapping
	public ResponseEntity<Permissao> criar(@Valid @RequestBody Permissao permissao, HttpServletResponse response) {
		Permissao permissaoSalvo = permissaoService.save(permissao);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, permissaoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(permissaoSalvo);
	}
	
	/**
	 * Atualizar Permissao
	 */
	@PutMapping("/{code}")
	public ResponseEntity<Permissao> atualizar(@PathVariable Long codigo, @Valid @RequestBody Permissao permissao) {
		Permissao permissaoSalva = permissaoService.atualizar(codigo, permissao);
		return ResponseEntity.ok(permissaoSalva);
	}
	

	/**
	 * Deletar Permissao
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		permissaoService.delete(code);
	}

}
