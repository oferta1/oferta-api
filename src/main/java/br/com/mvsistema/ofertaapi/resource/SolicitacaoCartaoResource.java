package br.com.mvsistema.ofertaapi.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.SolicitacaoCartaoFilter;
import br.com.mvsistema.ofertaapi.model.SolicitacaoCartao;
import br.com.mvsistema.ofertaapi.service.SolicitacaoCartaoService;


@RestController
@RequestMapping("/solicitacaoCartao")
public class SolicitacaoCartaoResource {
	
	@Autowired
	private SolicitacaoCartaoService solicitacaoCartaoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@PostMapping("/baixar")
	public ResponseEntity<?> baixarArquivo(@RequestBody SolicitacaoCartao solCartao) throws IOException {
		File file = new File(System.getProperty("user.home")+System.getProperty("file.separator")+solCartao.getNome()+".png");
        
        try ( FileOutputStream fos = new FileOutputStream(file); ) {
            byte[] decoder = Base64.getDecoder().decode(solCartao.getDocumento().replace("data:image/png;base64,", "").replace("data:image/jpeg;base64,", ""));
            fos.write(decoder);
          } catch (Exception e) {
            e.printStackTrace();
          }
		return ResponseEntity.ok(file);
	}
	
	
	/**
	 * Pesquisar SolicitacaoCartao
	 */
	@GetMapping
	public Page<SolicitacaoCartao> search(SolicitacaoCartaoFilter solicitacaoCartaoFilter, Pageable pageable) {
		return solicitacaoCartaoService.filter(solicitacaoCartaoFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o SolicitacaoCartao
	 */
	@GetMapping("/{code}")
	public ResponseEntity<SolicitacaoCartao> buscarPeloCodigo(@PathVariable Long code) {
		SolicitacaoCartao solicitacaoCartao = solicitacaoCartaoService.buscarSolicitacaoCartaoPeloCodigo(code);
		return solicitacaoCartao != null ? ResponseEntity.ok(solicitacaoCartao) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar SolicitacaoCartao
	 */
	@PostMapping
	public ResponseEntity<SolicitacaoCartao> criar(@Valid @RequestBody SolicitacaoCartao solicitacaoCartao, HttpServletResponse response) {
		SolicitacaoCartao solicitacaoCartaoSalvo = solicitacaoCartaoService.save(solicitacaoCartao);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, solicitacaoCartaoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(solicitacaoCartaoSalvo);
	}
	
	/**
	 * Atualizar SolicitacaoCartao
	 */
	@PutMapping("/{code}")
	public ResponseEntity<SolicitacaoCartao> atualizar(@PathVariable Long code, @Valid @RequestBody SolicitacaoCartao solicitacaoCartao) {
		SolicitacaoCartao solicitacaoCartaoSalva = solicitacaoCartaoService.atualizar(code, solicitacaoCartao);
		return ResponseEntity.ok(solicitacaoCartaoSalva);
	}
	

	/**
	 * Deletar SolicitacaoCartao
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		solicitacaoCartaoService.delete(code);
	}

}
