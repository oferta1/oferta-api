package br.com.mvsistema.ofertaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.NotificacaoFilter;
import br.com.mvsistema.ofertaapi.model.Notificacao;
import br.com.mvsistema.ofertaapi.service.NotificacaoService;


@RestController
@RequestMapping("/notificacao")
public class NotificacaoResource {
	
	@Autowired
	private NotificacaoService notificacaoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Notificacao
	 */
	@GetMapping
	public Page<Notificacao> search(NotificacaoFilter notificacaoFilter, Pageable pageable) {
		return notificacaoService.filter(notificacaoFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Notificacao
	 */
	@GetMapping("/{code}")
	public ResponseEntity<Notificacao> buscarPeloCodigo(@PathVariable Long code) {
		Notificacao notificacao = notificacaoService.buscarNotificacaoPeloCodigo(code);
		return notificacao != null ? ResponseEntity.ok(notificacao) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Notificacao
	 */
	@PostMapping
	public ResponseEntity<Notificacao> criar(@Valid @RequestBody Notificacao notificacao, HttpServletResponse response) {
		Notificacao notificacaoSalvo = notificacaoService.save(notificacao);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, notificacaoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(notificacaoSalvo);
	}
	
	/**
	 * Atualizar Notificacao
	 */
	@PutMapping("/{codigo}")
	public ResponseEntity<Notificacao> atualizar(@PathVariable Long codigo, @Valid @RequestBody Notificacao notificacao) {
		Notificacao notificacaoSalva = notificacaoService.atualizar(codigo, notificacao);
		return ResponseEntity.ok(notificacaoSalva);
	}
	

	/**
	 * Deletar Notificacao
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		notificacaoService.delete(code);
	}

}
