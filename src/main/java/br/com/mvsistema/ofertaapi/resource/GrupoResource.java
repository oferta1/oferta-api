package br.com.mvsistema.ofertaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvsistema.ofertaapi.event.RecursoCriadoEvent;
import br.com.mvsistema.ofertaapi.filter.GrupoFilter;
import br.com.mvsistema.ofertaapi.model.Grupo;
import br.com.mvsistema.ofertaapi.service.GrupoService;

@RestController
@RequestMapping("/grupo")
public class GrupoResource {
	
	@Autowired
	private GrupoService grupoService;

	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	/**
	 * Pesquisar Grupo
	 */
	@GetMapping
	public Page<Grupo> search(GrupoFilter grupoFilter, Pageable pageable) {
		return grupoService.filter(grupoFilter, pageable);
	}
	
	/**
	 * Buscar pelo codigo o Grupo
	 */
	@GetMapping("/{code}")
	public ResponseEntity<Grupo> buscarPeloCodigo(@PathVariable Long code) {
		Grupo grupo = grupoService.buscarGrupoPeloCodigo(code);
		return grupo != null ? ResponseEntity.ok(grupo) : ResponseEntity.notFound().build();
	}
	
	/**
	 * Criar Grupo
	 */
	@PostMapping
	public ResponseEntity<Grupo> criar(@Valid @RequestBody Grupo grupo, HttpServletResponse response) {
		Grupo grupoSalvo = grupoService.save(grupo);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, grupoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(grupoSalvo);
	}
	
	/**
	 * Atualizar Grupo
	 */
	@PutMapping("/{code}")
	public ResponseEntity<Grupo> atualizar(@PathVariable Long codigo, @Valid @RequestBody Grupo grupo) {
		Grupo grupoSalva = grupoService.atualizar(codigo, grupo);
		return ResponseEntity.ok(grupoSalva);
	}
	

	/**
	 * Deletar Grupo
	 */
	@DeleteMapping("/{code}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long code) {
		grupoService.delete(code);
	}

}
