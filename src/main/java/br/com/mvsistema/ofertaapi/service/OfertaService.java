package br.com.mvsistema.ofertaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.ofertaapi.model.Oferta;
import br.com.mvsistema.ofertaapi.repository.OfertaRepository;
import br.com.mvsistema.ofertaapi.filter.OfertaFilter;

@Service
public class OfertaService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private OfertaRepository ofertaRepository;

	@Transactional
	public Oferta save(Oferta oferta) {
		return ofertaRepository.save(oferta);
	}
	
	public Page<Oferta> filter(OfertaFilter ofertaFilter, Pageable pageable) {
		return ofertaRepository.filtrar(ofertaFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			ofertaRepository.deleteById(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Oferta buscarOfertaPeloCodigo(Long codigo) {
		Oferta ofertaSalva = ofertaRepository.findById(codigo).get();
		if (ofertaSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return ofertaSalva;
	}
	
	public Oferta atualizar(Long codigo, Oferta oferta) {
		Oferta ofertaSalva = buscarOfertaPeloCodigo(codigo);
		
		BeanUtils.copyProperties(oferta, ofertaSalva, "id");
		return ofertaRepository.save(ofertaSalva);
	}

}
