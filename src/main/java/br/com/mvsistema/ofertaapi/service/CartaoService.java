package br.com.mvsistema.ofertaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.ofertaapi.model.Cartao;
import br.com.mvsistema.ofertaapi.repository.CartaoRepository;
import br.com.mvsistema.ofertaapi.filter.CartaoFilter;

@Service
public class CartaoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private CartaoRepository cartaoRepository;

	@Transactional
	public Cartao save(Cartao cartao) {
		return cartaoRepository.save(cartao);
	}
	
	public Page<Cartao> filter(CartaoFilter cartaoFilter, Pageable pageable) {
		return cartaoRepository.filtrar(cartaoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			cartaoRepository.deleteById(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Cartao buscarCartaoPeloCodigo(Long codigo) {
		Cartao cartaoSalva = cartaoRepository.findById(codigo).get();
		if (cartaoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return cartaoSalva;
	}
	
	public Cartao buscarCartaoPeloCpfDoUsuario(String cpf) {
		Cartao cartaoUsuario= cartaoRepository.findByCartaoInUsuarioCpf(cpf);
		if (cartaoUsuario == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return cartaoUsuario;
	}
	
	public Cartao atualizar(Long codigo, Cartao cartao) {
		Cartao cartaoSalva = buscarCartaoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(cartao, cartaoSalva, "id");
		return cartaoRepository.save(cartaoSalva);
	}

}
