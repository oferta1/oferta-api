package br.com.mvsistema.ofertaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.ofertaapi.model.Notificacao;
import br.com.mvsistema.ofertaapi.repository.NotificacaoRepository;
import br.com.mvsistema.ofertaapi.filter.NotificacaoFilter;

@Service
public class NotificacaoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private NotificacaoRepository notificacaoRepository;

	@Transactional
	public Notificacao save(Notificacao notificacao) {
		return notificacaoRepository.save(notificacao);
	}
	
	public Page<Notificacao> filter(NotificacaoFilter notificacaoFilter, Pageable pageable) {
		return notificacaoRepository.filtrar(notificacaoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			notificacaoRepository.deleteById(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Notificacao buscarNotificacaoPeloCodigo(Long codigo) {
		Notificacao notificacaoSalva = notificacaoRepository.findById(codigo).get();
		if (notificacaoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return notificacaoSalva;
	}
	
	public Notificacao atualizar(Long codigo, Notificacao notificacao) {
		Notificacao notificacaoSalva = buscarNotificacaoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(notificacao, notificacaoSalva, "id");
		return notificacaoRepository.save(notificacaoSalva);
	}

}
