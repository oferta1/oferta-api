package br.com.mvsistema.ofertaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.ofertaapi.model.Grupo;
import br.com.mvsistema.ofertaapi.repository.GrupoRepository;
import br.com.mvsistema.ofertaapi.filter.GrupoFilter;

@Service
public class GrupoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private GrupoRepository grupoRepository;

	@Transactional
	public Grupo save(Grupo grupo) {
		return grupoRepository.save(grupo);
	}
	
	public Page<Grupo> filter(GrupoFilter grupoFilter, Pageable pageable) {
		return grupoRepository.filtrar(grupoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			grupoRepository.deleteById(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public Grupo buscarGrupoPeloCodigo(Long codigo) {
		Grupo grupoSalva = grupoRepository.findById(codigo).get();
		if (grupoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return grupoSalva;
	}
	
	public Grupo atualizar(Long codigo, Grupo grupo) {
		Grupo grupoSalva = buscarGrupoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(grupo, grupoSalva, "id");
		return grupoRepository.save(grupoSalva);
	}

}
