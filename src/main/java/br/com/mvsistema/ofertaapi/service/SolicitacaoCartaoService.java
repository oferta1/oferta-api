package br.com.mvsistema.ofertaapi.service;


import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.mvsistema.ofertaapi.model.SolicitacaoCartao;
import br.com.mvsistema.ofertaapi.repository.SolicitacaoCartaoRepository;
import br.com.mvsistema.ofertaapi.filter.SolicitacaoCartaoFilter;

@Service
public class SolicitacaoCartaoService {

	private String errorDelete = "this record is related to other tables.";

    @Autowired
	private SolicitacaoCartaoRepository solicitacaoCartaoRepository;

	@Transactional
	public SolicitacaoCartao save(SolicitacaoCartao solicitacaoCartao) {
		return solicitacaoCartaoRepository.save(solicitacaoCartao);
	}
	
	public Page<SolicitacaoCartao> filter(SolicitacaoCartaoFilter solicitacaoCartaoFilter, Pageable pageable) {
		return solicitacaoCartaoRepository.filtrar(solicitacaoCartaoFilter, pageable);
	}

	@Transactional
	public void delete(Long codigo) {
		try {
			solicitacaoCartaoRepository.deleteById(codigo);
		} catch (Exception e) {
			if (e instanceof org.hibernate.exception.ConstraintViolationException
					|| e instanceof DataIntegrityViolationException) {
				throw new IllegalArgumentException(errorDelete);
			}
			throw e;
		}

	}
	
	public SolicitacaoCartao buscarSolicitacaoCartaoPeloCodigo(Long codigo) {
		SolicitacaoCartao solicitacaoCartaoSalva = solicitacaoCartaoRepository.findById(codigo).get();
		if (solicitacaoCartaoSalva == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return solicitacaoCartaoSalva;
	}
	
	public SolicitacaoCartao atualizar(Long codigo, SolicitacaoCartao solicitacaoCartao) {
		SolicitacaoCartao solicitacaoCartaoSalva = buscarSolicitacaoCartaoPeloCodigo(codigo);
		
		BeanUtils.copyProperties(solicitacaoCartao, solicitacaoCartaoSalva, "id");
		return solicitacaoCartaoRepository.save(solicitacaoCartaoSalva);
	}

}
