package br.com.mvsistema.ofertaapi.filter;

import java.math.BigDecimal;
import java.math.BigDecimal;
import br.com.mvsistema.ofertaapi.model.PeriodoOferta;


public class OfertaFilter {

private Long id;
private String nomeProduto;
private String descricao;
private BigDecimal precoOferta;
private BigDecimal precoAnterior;
private String imagem;
private PeriodoOferta periodoOferta;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getNomeProduto() {
  return this.nomeProduto;
}
public void setNomeProduto(String nomeProduto) {
  this.nomeProduto = nomeProduto;
}
public String getDescricao() {
  return this.descricao;
}
public void setDescricao(String descricao) {
  this.descricao = descricao;
}
public BigDecimal getPrecoOferta() {
  return this.precoOferta;
}
public void setPrecoOferta(BigDecimal precoOferta) {
  this.precoOferta = precoOferta;
}
public BigDecimal getPrecoAnterior() {
  return this.precoAnterior;
}
public void setPrecoAnterior(BigDecimal precoAnterior) {
  this.precoAnterior = precoAnterior;
}
public String getImagem() {
  return this.imagem;
}
public void setImagem(String imagem) {
  this.imagem = imagem;
}
public PeriodoOferta getPeriodoOferta() {
  return this.periodoOferta;
}
public void setPeriodoOferta(PeriodoOferta periodoOferta) {
  this.periodoOferta = periodoOferta;
}



}
