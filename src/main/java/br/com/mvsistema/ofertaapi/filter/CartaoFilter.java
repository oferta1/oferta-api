package br.com.mvsistema.ofertaapi.filter;

import java.math.BigDecimal;

import br.com.mvsistema.ofertaapi.model.Usuario;

public class CartaoFilter {

	private Long id;
	private String numeroCartao;
	private String senhaCatao;
	private String nomeCliente;
	private BigDecimal valorFatura;
	private BigDecimal limite;
	private BigDecimal limiteDisponivel;
	private boolean status;
	private Usuario usuarioCartao;
	private String logoCartao;
	private String expiraEm;
	private String bancoCartao;

	public Usuario getUsuarioCartao() {
		return usuarioCartao;
	}

	public void setUsuarioCartao(Usuario usuarioCartao) {
		this.usuarioCartao = usuarioCartao;
	}

	public String getLogoCartao() {
		return logoCartao;
	}

	public void setLogoCartao(String logoCartao) {
		this.logoCartao = logoCartao;
	}

	public String getExpiraEm() {
		return expiraEm;
	}

	public void setExpiraEm(String expiraEm) {
		this.expiraEm = expiraEm;
	}

	public String getBancoCartao() {
		return bancoCartao;
	}

	public void setBancoCartao(String bancoCartao) {
		this.bancoCartao = bancoCartao;
	}
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroCartao() {
		return this.numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public String getSenhaCatao() {
		return this.senhaCatao;
	}

	public void setSenhaCatao(String senhaCatao) {
		this.senhaCatao = senhaCatao;
	}

	public String getNomeCliente() {
		return this.nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public BigDecimal getValorFatura() {
		return this.valorFatura;
	}

	public void setValorFatura(BigDecimal valorFatura) {
		this.valorFatura = valorFatura;
	}

	public BigDecimal getLimite() {
		return this.limite;
	}

	public void setLimite(BigDecimal limite) {
		this.limite = limite;
	}

	public BigDecimal getLimiteDisponivel() {
		return this.limiteDisponivel;
	}

	public void setLimiteDisponivel(BigDecimal limiteDisponivel) {
		this.limiteDisponivel = limiteDisponivel;
	}

	public boolean isStatus() {
		return this.status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
