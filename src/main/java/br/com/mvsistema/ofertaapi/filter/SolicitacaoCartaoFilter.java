package br.com.mvsistema.ofertaapi.filter;

import java.time.LocalDate;
import java.math.BigDecimal;


public class SolicitacaoCartaoFilter {

private Long id;
private String nome;
private String email;
private String cpf;
private LocalDate dataNascimento;
private String telefone;
private String profissao;
private BigDecimal rendaMensal;
private String documento;
private String cep;
private String logradouro;
private String complemento;
private String bairro;
private String localidade;
private String numero;
private String uf;
private boolean status;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getNome() {
  return this.nome;
}
public void setNome(String nome) {
  this.nome = nome;
}
public String getEmail() {
  return this.email;
}
public void setEmail(String email) {
  this.email = email;
}
public String getCpf() {
  return this.cpf;
}
public void setCpf(String cpf) {
  this.cpf = cpf;
}
public LocalDate getDataNascimento() {
  return this.dataNascimento;
}
public void setDataNascimento(LocalDate dataNascimento) {
  this.dataNascimento = dataNascimento;
}
public String getTelefone() {
  return this.telefone;
}
public void setTelefone(String telefone) {
  this.telefone = telefone;
}
public String getProfissao() {
  return this.profissao;
}
public void setProfissao(String profissao) {
  this.profissao = profissao;
}
public BigDecimal getRendaMensal() {
  return this.rendaMensal;
}
public void setRendaMensal(BigDecimal rendaMensal) {
  this.rendaMensal = rendaMensal;
}
public String getDocumento() {
  return this.documento;
}
public void setDocumento(String documento) {
  this.documento = documento;
}
public String getCep() {
  return this.cep;
}
public void setCep(String cep) {
  this.cep = cep;
}
public String getLogradouro() {
  return this.logradouro;
}
public void setLogradouro(String logradouro) {
  this.logradouro = logradouro;
}
public String getComplemento() {
  return this.complemento;
}
public void setComplemento(String complemento) {
  this.complemento = complemento;
}
public String getBairro() {
  return this.bairro;
}
public void setBairro(String bairro) {
  this.bairro = bairro;
}
public String getLocalidade() {
  return this.localidade;
}
public void setLocalidade(String localidade) {
  this.localidade = localidade;
}
public String getNumero() {
  return this.numero;
}
public void setNumero(String numero) {
  this.numero = numero;
}
public String getUf() {
  return this.uf;
}
public void setUf(String uf) {
  this.uf = uf;
}
public boolean isStatus() {
  return this.status;
}
public void setStatus(boolean status) {
  this.status = status;
}



}
