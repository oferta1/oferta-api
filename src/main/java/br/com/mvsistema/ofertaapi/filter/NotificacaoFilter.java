package br.com.mvsistema.ofertaapi.filter;

import java.time.LocalDate;


public class NotificacaoFilter {

private Long id;
private String nome;
private String descricao;
private LocalDate data;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getNome() {
  return this.nome;
}
public void setNome(String nome) {
  this.nome = nome;
}
public String getDescricao() {
  return this.descricao;
}
public void setDescricao(String descricao) {
  this.descricao = descricao;
}
public LocalDate getData() {
  return this.data;
}
public void setData(LocalDate data) {
  this.data = data;
}



}
