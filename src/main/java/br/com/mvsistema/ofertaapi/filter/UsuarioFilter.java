package br.com.mvsistema.ofertaapi.filter;

import java.util.List;
import java.util.List;


public class UsuarioFilter {

private Long id;
private String nome;
private String cpf;
private String email;
private String senha;
private boolean ativo;
private List permissoes;


public Long getId() {
  return this.id;
}
public void setId(Long id) {
  this.id = id;
}
public String getNome() {
  return this.nome;
}
public void setNome(String nome) {
  this.nome = nome;
}
public String getCpf() {
  return this.cpf;
}
public void setCpf(String cpf) {
  this.cpf = cpf;
}
public String getEmail() {
  return this.email;
}
public void setEmail(String email) {
  this.email = email;
}
public String getSenha() {
  return this.senha;
}
public void setSenha(String senha) {
  this.senha = senha;
}
public boolean isAtivo() {
  return this.ativo;
}
public void setAtivo(boolean ativo) {
  this.ativo = ativo;
}
public List getPermissoes() {
  return this.permissoes;
}
public void setPermissoes(List permissoes) {
  this.permissoes = permissoes;
}

}
